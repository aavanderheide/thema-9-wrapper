import weka.classifiers.lazy.IBk;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.IOException;

public class AlternativeIBK {
    private final String modelFile = "testdata/IBKmodel.model";

    public static void main(String[] args) {
        AlternativeIBK runner = new AlternativeIBK();
        runner.start();
    }

    private void start() {
        String datafile = "testdata/ProjectDatasetConflicting.arff";
        String correctingFile = "testdata/ProjectDatasetNon-Conflicting.arff";
        try {
            IBk ibk = new IBk();
            Instances instances = loadArff(datafile);
            printInstances(instances);
            ibk.buildClassifier(instances);
            saveClassifier(ibk);
            //loads model
            IBk fromFile = loadClassifier();
            Instances unknownInstances = loadArff(correctingFile);
            classifyNewInstance(fromFile, unknownInstances);
            // set class index to last attribute
            makePrediction(instances, fromFile);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void classifyNewInstance(IBk ibk, Instances unknownInstances) throws Exception {
        // creating a copy
        Instances labeled = new Instances(unknownInstances);
        // label all the instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = ibk.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }

    private IBk loadClassifier() throws Exception {
        // deserialize model
        return (IBk) weka.core.SerializationHelper.read(modelFile);
    }

    private void saveClassifier(IBk ibk) throws Exception {
        //post 3.5.5
        // serialize model
        weka.core.SerializationHelper.write(modelFile, ibk);
    }

    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }

        System.out.println("class index = " + instances.classIndex());

        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }
    }

    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            // This will set the class attribute if this is not present in the data format
            // There are some format which does this.For example, the XRFF format saves the class attribute information.
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }

    private void makePrediction(Instances instances, IBk fromFile) throws Exception {
        int count = 0;
        int lineInFile;
        for(int i = 0; i < instances.numInstances(); i++){
            Instance inst = instances.instance(i);
            double actualClassValue  = instances.instance(i).classValue();
            //will print the class value
            String actual=instances.classAttribute().value((int)actualClassValue);
            double result = fromFile.classifyInstance(inst);
            //will print the predicted value
            String prediction=instances.classAttribute().value((int)result );
            if (result != actualClassValue) {
                count += 1;
                lineInFile = i + 9;
                System.out.println("Line " + lineInFile +", " + "Wrongly classified: " + inst);
                System.out.println("Line " + lineInFile +", " + "Model predicts : "
                        + prediction + ", but is actually : " + actual);
                System.out.println(" ");
            }
        }
        System.out.println("Number of different predictions " + count);
    }
}