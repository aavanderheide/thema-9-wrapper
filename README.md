# Genetic Variant Classifications #

Author: Aaron van der Heide.  
Version: 1.0  
Date: 16-11-2020  
Student Bioinformatics at Hanzehogeschool Groningen.  

### Repository info ###

* This repository contains a Java wrapper which uses a model build in Weka to predict if a certain mutation is; benign, possibly damaging, probably damaging or malignant. At first a Vote model would have been used but due to technical issues IBk is used as classifier.  
* Version: 1.0  


### Set up ###

* This repository has to be cloned into IntelliJ IDEA 2020.2.1
* - uses JDK: 14 java version 14.0.2
* - language level 8
* - (both these settings can be changed at: From the main menu, select File | Project Structure | Project Settings | Project.
* - needs two arff for comparing and classification, one for only classification
* - note: if chosen for only classification the following lines have to be hashed out (with // before each line): 18, 27 and 28.
  
* The wrapper works according the following steps:
* - a file with testdata must be entered at line 17 between the "". (default is ProjectDatasetConflicting.arff)
* - a file with trainingsdata must be entered at line 18 between the "". (default is ProjectDatasetnon-Conflicting.arff)
* - run the wrapper
  
  
* What does it?
* - from the trainingsdata file all the instances get created.
* - the instances from the testdata file get compared to them of the trainingsdata. 
* - the instances get compared and the ones who do not seem to be right get printed to screen with the right prediction by IBk
* - by looking at which combination is most common (IBk) it is reassigned to that instance (on screen).
